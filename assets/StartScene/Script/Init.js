
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/StartScene/Script/Init.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0092eunB9JGUZtzU4oC8c7T', 'Init');
// StartScene/Script/Init.ts

Object.defineProperty(exports, "__esModule", { value: true });
var GamePlatformType_1 = require("../../myGame/Script/Platform/GamePlatformType");
var GamePlatform_1 = require("../../myGame/Script/Platform/GamePlatform");
var GamePlatformConfig_1 = require("../../myGame/Script/Platform/GamePlatformConfig");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Init = /** @class */ (function (_super) {
    __extends(Init, _super);
    function Init() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.type = GamePlatformType_1.GamePlatformType.PC;
        _this.platformConfig = null;
        _this.Plan = null;
        return _this;
    }
    Init.prototype.onLoad = function () {
        console.log('>>>当前平台', GamePlatformType_1.GamePlatformType[this.type]);
        GamePlatform_1.default.instance.init(this.copyConfig(this.platformConfig.children[this.type].getComponent(GamePlatformConfig_1.default)));
    };
    Init.prototype.start = function () {
        // Loader.loadSubpackage("Level", null, false);
        // Loader.loadSubpackage("Skin", null, false);
        // setTimeout(this.loadMainScene.bind(this), 100);
        this.loadMainScene();
        // Loader.loadSubpackage("Level", () => {
        //     Loader.loadSubpackage("PlayerHead", null, false);
        //     Loader.loadSubpackage("Skin", this.loadMainScene.bind(this), false)
        // }, false);
    };
    Init.prototype.loadMainScene = function () {
        var _this = this;
        var loader_ = function (completedCount, totalCount, item) {
            var jindu = completedCount / totalCount;
            _this.Plan.progress = jindu;
        };
        cc.director.preloadScene("MainScene", loader_, function (errs) {
            if (errs) {
                cc.log(errs);
                cc.director.loadScene("Init");
                return;
            }
            cc.log("加载游戏主场景完成");
            cc.director.loadScene("MainScene");
        });
    };
    Init.prototype.copyConfig = function (cfg) {
        var data = new GamePlatformConfig_1.default();
        data.type = JSON.parse(JSON.stringify(cfg.type));
        data.appId = JSON.parse(JSON.stringify(cfg.appId));
        data.secret = JSON.parse(JSON.stringify(cfg.secret));
        data.ServiceAdress = JSON.parse(JSON.stringify(cfg.ServiceAdress));
        data.videoAdUnitId = JSON.parse(JSON.stringify(cfg.videoAdUnitId));
        data.BannerAdUnitId = JSON.parse(JSON.stringify(cfg.BannerAdUnitId));
        data.InterstitialAdUnitId = JSON.parse(JSON.stringify(cfg.InterstitialAdUnitId));
        data.share = JSON.parse(JSON.stringify(cfg.share));
        data.video = JSON.parse(JSON.stringify(cfg.video));
        data.banner = JSON.parse(JSON.stringify(cfg.banner));
        data.interstitial = JSON.parse(JSON.stringify(cfg.interstitial));
        data.vibrate = JSON.parse(JSON.stringify(cfg.vibrate));
        return data;
    };
    __decorate([
        property({ type: cc.Enum(GamePlatformType_1.GamePlatformType) })
    ], Init.prototype, "type", void 0);
    __decorate([
        property(cc.Node)
    ], Init.prototype, "platformConfig", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], Init.prototype, "Plan", void 0);
    Init = __decorate([
        ccclass
    ], Init);
    return Init;
}(cc.Component));
exports.default = Init;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU3RhcnRTY2VuZVxcU2NyaXB0XFxJbml0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxrRkFBaUY7QUFDakYsMEVBQXFFO0FBQ3JFLHNGQUFpRjtBQUczRSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWtDLHdCQUFZO0lBRDlDO1FBQUEscUVBd0RDO1FBckRVLFVBQUksR0FBcUIsbUNBQWdCLENBQUMsRUFBRSxDQUFDO1FBRzdDLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBR3RDLFVBQUksR0FBbUIsSUFBSSxDQUFDOztJQStDaEMsQ0FBQztJQTdDRyxxQkFBTSxHQUFOO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsbUNBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDcEQsc0JBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksQ0FBQyw0QkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxSCxDQUFDO0lBQ0Qsb0JBQUssR0FBTDtRQUNJLCtDQUErQztRQUMvQyw4Q0FBOEM7UUFDOUMsa0RBQWtEO1FBQ2xELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQix5Q0FBeUM7UUFDekMsd0RBQXdEO1FBQ3hELDBFQUEwRTtRQUMxRSxhQUFhO0lBQ2pCLENBQUM7SUFDRCw0QkFBYSxHQUFiO1FBQUEsaUJBY0M7UUFiRyxJQUFJLE9BQU8sR0FBRyxVQUFDLGNBQWMsRUFBRSxVQUFVLEVBQUUsSUFBSTtZQUMzQyxJQUFJLEtBQUssR0FBRyxjQUFjLEdBQUcsVUFBVSxDQUFDO1lBQ3hDLEtBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUMvQixDQUFDLENBQUE7UUFDRCxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsT0FBTyxFQUFFLFVBQUMsSUFBSTtZQUNoRCxJQUFJLElBQUksRUFBRTtnQkFDTixFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNiLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QixPQUFPO2FBQ1Y7WUFDRCxFQUFFLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3BCLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELHlCQUFVLEdBQVYsVUFBVyxHQUF1QjtRQUM5QixJQUFJLElBQUksR0FBRyxJQUFJLDRCQUFrQixFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFwREQ7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxtQ0FBZ0IsQ0FBQyxFQUFFLENBQUM7c0NBQ007SUFHcEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDb0I7SUFHdEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQztzQ0FDRztJQVJYLElBQUk7UUFEeEIsT0FBTztPQUNhLElBQUksQ0F1RHhCO0lBQUQsV0FBQztDQXZERCxBQXVEQyxDQXZEaUMsRUFBRSxDQUFDLFNBQVMsR0F1RDdDO2tCQXZEb0IsSUFBSSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEdhbWVQbGF0Zm9ybVR5cGUgfSBmcm9tIFwiLi4vLi4vbXlHYW1lL1NjcmlwdC9QbGF0Zm9ybS9HYW1lUGxhdGZvcm1UeXBlXCI7XG5pbXBvcnQgR2FtZVBsYXRmb3JtIGZyb20gXCIuLi8uLi9teUdhbWUvU2NyaXB0L1BsYXRmb3JtL0dhbWVQbGF0Zm9ybVwiO1xuaW1wb3J0IEdhbWVQbGF0Zm9ybUNvbmZpZyBmcm9tIFwiLi4vLi4vbXlHYW1lL1NjcmlwdC9QbGF0Zm9ybS9HYW1lUGxhdGZvcm1Db25maWdcIjtcbmltcG9ydCBMb2FkZXIgZnJvbSBcIi4uLy4uL215R2FtZS9TY3JpcHQvQ29tbW9uL0xvYWRlclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSW5pdCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShHYW1lUGxhdGZvcm1UeXBlKSB9KVxuICAgIHB1YmxpYyB0eXBlOiBHYW1lUGxhdGZvcm1UeXBlID0gR2FtZVBsYXRmb3JtVHlwZS5QQztcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIHB1YmxpYyBwbGF0Zm9ybUNvbmZpZzogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuUHJvZ3Jlc3NCYXIpXG4gICAgUGxhbjogY2MuUHJvZ3Jlc3NCYXIgPSBudWxsO1xuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICBjb25zb2xlLmxvZygnPj4+5b2T5YmN5bmz5Y+wJywgR2FtZVBsYXRmb3JtVHlwZVt0aGlzLnR5cGVdKTtcbiAgICAgICAgR2FtZVBsYXRmb3JtLmluc3RhbmNlLmluaXQodGhpcy5jb3B5Q29uZmlnKHRoaXMucGxhdGZvcm1Db25maWcuY2hpbGRyZW5bdGhpcy50eXBlXS5nZXRDb21wb25lbnQoR2FtZVBsYXRmb3JtQ29uZmlnKSkpO1xuICAgIH1cbiAgICBzdGFydCgpIHtcbiAgICAgICAgLy8gTG9hZGVyLmxvYWRTdWJwYWNrYWdlKFwiTGV2ZWxcIiwgbnVsbCwgZmFsc2UpO1xuICAgICAgICAvLyBMb2FkZXIubG9hZFN1YnBhY2thZ2UoXCJTa2luXCIsIG51bGwsIGZhbHNlKTtcbiAgICAgICAgLy8gc2V0VGltZW91dCh0aGlzLmxvYWRNYWluU2NlbmUuYmluZCh0aGlzKSwgMTAwKTtcbiAgICAgICAgdGhpcy5sb2FkTWFpblNjZW5lKCk7XG4gICAgICAgIC8vIExvYWRlci5sb2FkU3VicGFja2FnZShcIkxldmVsXCIsICgpID0+IHtcbiAgICAgICAgLy8gICAgIExvYWRlci5sb2FkU3VicGFja2FnZShcIlBsYXllckhlYWRcIiwgbnVsbCwgZmFsc2UpO1xuICAgICAgICAvLyAgICAgTG9hZGVyLmxvYWRTdWJwYWNrYWdlKFwiU2tpblwiLCB0aGlzLmxvYWRNYWluU2NlbmUuYmluZCh0aGlzKSwgZmFsc2UpXG4gICAgICAgIC8vIH0sIGZhbHNlKTtcbiAgICB9XG4gICAgbG9hZE1haW5TY2VuZSgpIHtcbiAgICAgICAgbGV0IGxvYWRlcl8gPSAoY29tcGxldGVkQ291bnQsIHRvdGFsQ291bnQsIGl0ZW0pID0+IHtcbiAgICAgICAgICAgIGxldCBqaW5kdSA9IGNvbXBsZXRlZENvdW50IC8gdG90YWxDb3VudDtcbiAgICAgICAgICAgIHRoaXMuUGxhbi5wcm9ncmVzcyA9IGppbmR1O1xuICAgICAgICB9XG4gICAgICAgIGNjLmRpcmVjdG9yLnByZWxvYWRTY2VuZShcIk1haW5TY2VuZVwiLCBsb2FkZXJfLCAoZXJycykgPT4ge1xuICAgICAgICAgICAgaWYgKGVycnMpIHtcbiAgICAgICAgICAgICAgICBjYy5sb2coZXJycyk7XG4gICAgICAgICAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKFwiSW5pdFwiKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYy5sb2coXCLliqDovb3muLjmiI/kuLvlnLrmma/lrozmiJBcIik7XG4gICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJNYWluU2NlbmVcIik7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBjb3B5Q29uZmlnKGNmZzogR2FtZVBsYXRmb3JtQ29uZmlnKSB7XG4gICAgICAgIGxldCBkYXRhID0gbmV3IEdhbWVQbGF0Zm9ybUNvbmZpZygpO1xuICAgICAgICBkYXRhLnR5cGUgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGNmZy50eXBlKSk7XG4gICAgICAgIGRhdGEuYXBwSWQgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGNmZy5hcHBJZCkpO1xuICAgICAgICBkYXRhLnNlY3JldCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoY2ZnLnNlY3JldCkpO1xuICAgICAgICBkYXRhLlNlcnZpY2VBZHJlc3MgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGNmZy5TZXJ2aWNlQWRyZXNzKSk7XG4gICAgICAgIGRhdGEudmlkZW9BZFVuaXRJZCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoY2ZnLnZpZGVvQWRVbml0SWQpKTtcbiAgICAgICAgZGF0YS5CYW5uZXJBZFVuaXRJZCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoY2ZnLkJhbm5lckFkVW5pdElkKSk7XG4gICAgICAgIGRhdGEuSW50ZXJzdGl0aWFsQWRVbml0SWQgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGNmZy5JbnRlcnN0aXRpYWxBZFVuaXRJZCkpO1xuICAgICAgICBkYXRhLnNoYXJlID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShjZmcuc2hhcmUpKTtcbiAgICAgICAgZGF0YS52aWRlbyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoY2ZnLnZpZGVvKSk7XG4gICAgICAgIGRhdGEuYmFubmVyID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShjZmcuYmFubmVyKSk7XG4gICAgICAgIGRhdGEuaW50ZXJzdGl0aWFsID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShjZmcuaW50ZXJzdGl0aWFsKSk7XG4gICAgICAgIGRhdGEudmlicmF0ZSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoY2ZnLnZpYnJhdGUpKTtcbiAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxufSJdfQ==